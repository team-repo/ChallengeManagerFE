# Notice
This repository is deprecated and should not be modified or added to. It will soon be deleted after all team members have moved their code changes to the new single repository:
https://gitlab.com/team-repo/challenge-manager

## TODSS-JCR
This is the repository for the front-end part of the TODSS JCR Challenge Manager.
