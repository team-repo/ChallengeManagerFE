import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class ChallengePopupComponent extends LitElement {

    constructor() {
        super();
        this.open = false;
    }

    static get properties() {
        return {
            open: {type: Boolean},
            challengeData: {type: JSON}
        }
    }

    static get styles() {
        return css`
          .modal.open {
            display: block;
            position: fixed;
            z-index: 1;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            color: black;
            background-color: rgb(0, 0, 0);
            background-color: rgba(0, 0, 0, 0.4);
            visibility: visible;
          }

          .modal:not(.open) {
            display: none; /* Hidden by default */
          }

          /* Modal Content/Box */

          .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 20px;
            border: 15px;
            width: 80%; /* Could be more or less, depending on screen size */
            border-radius: 10px;
          }

          /* The Close Button */

          .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
          }

          .close:hover,
          .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
          }

          .information {
            display: flex;
            flex-direction: column;
          }
        `;
    }

    render() {
        let openClosed = this.open ? "modal open" : "modal";

        return html`
            <div id="myModal" class="${openClosed}">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <div class="information">
                        ${Object.entries(this.challengeData).map(challengeDataPoint => {
                            return html`
                                <div class="information-sub">
                                    <strong>${challengeDataPoint[0]}</strong>: ${challengeDataPoint[1]}
                                </div>
                            `
                        })}
                    </div>
                </div>
            </div>
        `;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);

        let closeButton = this.shadowRoot.querySelector(".close");
        closeButton.addEventListener("click", () => {
            this.open = false;

            // Custom event to let ChallengeComponent know its information popup has closed.
            this.modalClosedEvent = new CustomEvent("modalClosed", {
                bubbles: true,
                composed: true,
                cancelable: false,
            });
            this.dispatchEvent(this.modalClosedEvent);
        });
    }

}

customElements.define('challenge-popup', ChallengePopupComponent);