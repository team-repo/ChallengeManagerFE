import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class DataviewComponent extends LitElement {

    static get properties() {
        return {
            challengeList: {type: Array}
        }
    }

    static get styles() {
        return css`
          .data-view-container {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
            background-color: #01102c;
            margin: 1rem 10rem;
            padding-top: 1rem;
          }
        `;
    }

    render() {
        if (this.challengeList === null || this.challengeList === undefined) return html``;

        return html`
            <div class="data-view-container">
                ${this.challengeList.map(challenge => {
                    return html`
                        <challenge-component .challengeData="${challenge}"></challenge-component>
                    `;
                })}
            </div>
        `;
    }
}

customElements.define('data-view', DataviewComponent);