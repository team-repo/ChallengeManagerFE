import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class ApplicationComponent extends LitElement {
    exportChallengeList = [];

    static get properties() {
        return {
            challengeList: {type: JSON}
        }
    }

    static get styles() {
        return css`
          #overwriteDiv label {
            display: inline;
          }
          
          #upload-button {
            cursor: pointer;
            border-radius: 0.75rem;
            --tw-bg-opacity: 1;
            background-color: rgba(237,37,78,var(--tw-bg-opacity));
            padding: 0.875rem 2rem 0.875rem 2rem;
            font-family: Conthrax,sans-serif,Arial;
            font-size: 16px;
            --tw-text-opacity: 1;
            color: rgba(255,255,255,var(--tw-text-opacity));
            border: 0;
            margin-left: 0.75rem;
          }

          input[type="file"] {
            display: none;
          }
          .custom-file-upload {
            cursor: pointer;
            border-radius: 0.75rem;
            --tw-bg-opacity: 1;
            background-color: rgba(237,37,78,var(--tw-bg-opacity));
            padding: 0.875rem 2rem 0.875rem 2rem;
            font-family: Conthrax,sans-serif,Arial;
            font-size: 16px;
          }
          
          #search-form input{
            cursor: pointer;
            border-radius: 0.75rem;
            --tw-bg-opacity: 1;
            padding: 0.875rem 2rem 0.875rem 2rem;
            font-family: Conthrax,sans-serif,Arial;
            font-size: 16px;
            border: 3px solid rgba(237,37,78,var(--tw-bg-opacity));
            margin-bottom: 0.5rem;
            color: black;
          }
          
          #overwriteDiv {
            margin-bottom: 0.5rem;
            margin-top: 0.2rem;
          }
        `;
    }

    render() {
        return html`
            <div class="menu">
                <form id="search-form">
                    <div class="topnav">
                        <input type="text" placeholder="Search..">
                    </div>
                </form>
                
                <label for="uploadFile" class="custom-file-upload">
                    <i class="fa fa-cloud-upload"></i> Choose File
                </label>
                <input type="file" id="uploadFile">
                
                <button id="upload-button"> Upload </button>
                <div id="overwriteDiv">
                    <input type="checkbox" id="overwriteCheckBox" name="Overwrite" value="bread">
                    <label for="overwriteCheckBox">Overwrite previous challenges</label><br>
                </div>
                <button id="export-button"> Export </button>
            </div>

            <data-view .challengeList="${this.challengeList}"></data-view>
        `;
    }

    firstUpdated(_changedProperties) {
        super.firstUpdated(_changedProperties);

        let uploadButton = this.shadowRoot.querySelector("#uploadFile");
        uploadButton.onchange = () => {
            this._uploadFile();
        }

        this.shadowRoot.addEventListener("isCheckedEvent", (event) => {
            this.exportChallengeList.push(event.detail)
        })

        this.shadowRoot.addEventListener("isUncheckedEvent", (event) => {
            for (let i = 0; i < this.exportChallengeList.length; i++) {
                if (this.exportChallengeList[i] === event.detail) {
                    this.exportChallengeList.splice(i, 1)
                }
            }
        })

        this.shadowRoot.querySelector("#export-button").addEventListener("click", () => {
            this._exportChallenges(this.exportChallengeList)
            console.log(this.exportChallengeList)
        })
    }

    _uploadFile() {
        let myUploadedFile = this.shadowRoot.querySelector("#uploadFile").files[0];
        let formData = new FormData();
        formData.append("file", myUploadedFile);
        fetch('http://localhost:8080/ctfdexport/upload', {
            method: "POST",
            body: formData
        }).then(response => response.json())
            .then(data => {
                let checked = this.shadowRoot.querySelector("#overwriteCheckBox").checked

                if (checked || this.challengeList === undefined) {
                    this.challengeList = data;
                } else {
                    this.challengeList = this.challengeList.concat(data)
                }
            })
    }

    _exportChallenges(challenges) {
        fetch('http://localhost:8080/ctfdexport/export', {
            method: "POST",
            headers: new Headers({'content-type': 'application/json'}),
            body: JSON.stringify({count:challenges.length, results: challenges})
        })
    }

}

customElements.define('application-component', ApplicationComponent);